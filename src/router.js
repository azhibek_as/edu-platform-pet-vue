import { createRouter, createWebHistory } from 'vue-router';
import HomePage from './views/HomePage.vue';
import CoursePage from './views/CoursePage.vue';
import NotFoundPage from './views/NotFoundPage.vue';
import LessonPage from "./views/LessonPage.vue";

const routes = [
    { path: '/', name: 'Home', component: HomePage },
    { path: '/courses/:id', name: 'Course', component: CoursePage, props: true },
    { path: '/courses/:courseId/lessons/:lessonId', name: 'Lesson', component: LessonPage, props: true },
    { path: '/:pathMatch(.*)*', name: 'NotFound', component: NotFoundPage } // Обработка несуществующих маршрутов
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;
