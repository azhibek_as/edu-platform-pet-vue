import { defineStore } from 'pinia';

export const useCourseStore = defineStore('courseStore', {
    state: () => ({
        courses: [
            // Исходные данные курсов
        ]
    }),
    actions: {
        addCourse(course) {
            this.courses.push(course);
        },
        addLesson(courseId, lesson) {
            const course = this.courses.find(c => c.id === courseId);
            if (course) {
                if (!course.lessons) {
                    course.lessons = [];
                }
                course.lessons.push(lesson);
            }
        },
        addTest(courseId, lessonId, test) {
            const course = this.courses.find(c => c.id === courseId);
            const lesson = course?.lessons.find(l => l.id === lessonId);
            if (lesson) {
                if (!lesson.tests) {
                    lesson.tests = [];
                }
                lesson.tests.push(test);
            }
        }
    }
});